/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author peta_zetas
 */
public class MaquinaDeEstados {
    private Integer estadoActual;
    private Automata automata;

    public MaquinaDeEstados(Automata automata) {
        this.automata = automata;
    }
    
    public void inicializar(){
        
        automata.cargarAlfabeto();
        automata.cargarEstados();
        automata.cargarEstadosFinales();
        automata.establecerQi(0);
        automata.inicializaMatriz();
        estadoActual = automata.getEstadoInicial();
    }
    
    //Modifica el estado actual al siguiente estado al que se llega una vez metido un caracter
    public void acepta(Character caracter) throws Exception{
        Integer estadoTemporal = automata.getSiguienteEstado(estadoActual, caracter);
        if(estadoTemporal!=null){
            estadoActual = estadoTemporal;
            //System.out.println("Estado -> " + estadoActual);
        }
        else{
            System.out.println("No hay cambio de estado");
            throw new Exception(); //lanzar excepcion y parar automaticamente el procedimiento de comprobar la cadena
        }
    }
    
    public boolean esFinal(){
        return automata.esFinal(estadoActual);
    }
    
    //Comprueba la cadena de texto
    public boolean compruebaCadena(String cadena){
        
        estadoActual = 0;  //Por si estuviera en otro estado
        int posicion = 0;
        try{
            while(posicion < cadena.length()){
                char letra = cadena.charAt(posicion);
                this.acepta(letra);
                posicion++;
            }
            return this.esFinal();
        }catch(Exception ex){}
            return false;   
        
    }
    
    public boolean parar() {
        if(esFinal()) {  
            return new Random().nextBoolean(); 
        }else{
            return false;  
        }
    }
    
    public ArrayList<String> generarCadenas(int numeroCadenas, int limiteCadena){
        
        System.out.println("-----GENERADOR DE CADENAS-----");
        
        ArrayList<String> cadenas = new ArrayList<>();
        int generadas = 0;
 
        while(generadas<numeroCadenas) {
            
            estadoActual = 0;  
            String cadena = "";
            while(!parar() && cadena.length()<limiteCadena && automata.continuar(estadoActual)){
                String letra = automata.añadeLetraRandom(estadoActual);
                cadena = cadena + letra;
                estadoActual = automata.getSiguienteEstado(estadoActual, (Character) letra.charAt(0));
            }
            if(compruebaCadena(cadena) && !cadenas.contains(cadena)){
                cadenas.add(cadena);
                generadas++;
            } 
        }
        return cadenas;
    }
}