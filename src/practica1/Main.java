/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author peta_zetas
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Automata automata = new Automata();
        MaquinaDeEstados maquinaDeEstados = new MaquinaDeEstados(automata);
        maquinaDeEstados.inicializar();
        String peticion = pedirOpcion(); 
        
        if("1".equals(peticion)){
            System.out.println("COMPROBAR CADENA");
            String cadena = pedirCadena();
            boolean solucion = maquinaDeEstados.compruebaCadena(cadena);
            System.out.println(solucion);
        }
        else if("2".equals(peticion)){
            ArrayList<String> maquina = maquinaDeEstados.generarCadenas(100, 10);
            for(int i = 0; i < maquina.size(); i++) {   
                System.out.print(maquina.get(i)+"\n");
            }           
         
        }
        else{
            System.out.println("Opcion no encontrada");
        }
        
    }
    
    public static String pedirOpcion(){
        String peticion; 
        Scanner sc = new Scanner(System.in);
        System.out.println("--------------------------------------------------------------------------------------");
        System.out.println("¡Hola! Introduce 1 para validar expresión o 2 para generar expresión"); 
        System.out.println("--------------------------------------------------------------------------------------");
        peticion = sc.nextLine();  
        return peticion;
    }
    public static String pedirCadena(){
        String peticion; 
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduce la cadena: "); 
        peticion = sc.nextLine();  
        return peticion;
    }
}