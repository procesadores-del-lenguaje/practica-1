/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 *
 * @author peta_zetas
 */
public class Automata {
    
    private ArrayList<Character> alfabeto = new ArrayList<Character>();
    private ArrayList<Integer> estados = new ArrayList<Integer>();
    private ArrayList<Integer> estadosFinales = new ArrayList<Integer>();
    private HashMap<Integer, HashMap<Character, Integer>> matriz = new HashMap<Integer, HashMap<Character, Integer>>();
    private int EstadoInicial;

    public Automata() {
    }

    public int getEstadoInicial() {
        return EstadoInicial;
    }

    public void cargarAlfabeto(){
        
        List<Character> alfabeto = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm','n', 'o','p','q','r','s','t','u','v','w','x','y','z', 'A', 'B', 'C', 'D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9');
        
        for (int i = 0; i < alfabeto.size(); i++) {
            this.alfabeto.add(alfabeto.get(i));
        } 
    }
    
    //Carga los estados en la lista
    public void  cargarEstados(){
            
        int numeroEstados = 17;
        for (int i = 0; i < numeroEstados; i++) {
            estados.add(i);
        }
    }
    
    public void cargarEstadosFinales(){ 
        int[] finales = {4, 6, 7, 8, 9, 11, 13, 14, 15, 16};
        for (int i = 0; i < finales.length; i++) {
            estadosFinales.add(finales[i]);
        }
        //System.out.println("Finales= "+ estadosFinales.toString());
    }
    
    public void establecerQi(Integer estado){
        EstadoInicial = estado;
        System.out.println("Estado inicial = ");
    }
    
    public void inicializaMatriz(){
        
        for (int i = 0; i < estados.size(); i++) {
            matriz.put(estados.get(i), new HashMap <Character, Integer>());
        }
        
        matriz.get(0).put(new Character('a'), new Integer(1));
        matriz.get(1).put(new Character('a'), new Integer(2)); 
        matriz.get(1).put(new Character('b'), new Integer(3)); 
        matriz.get(2).put(new Character('b'), new Integer(3));
        matriz.get(3).put(new Character('c'), new Integer(5));
        matriz.get(3).put(new Character('m'), new Integer(9));   
        matriz.get(3).put(new Character('n'), new Integer(6));
        matriz.get(3).put(new Character('o'), new Integer(8));   
        matriz.get(3).put(new Character('p'), new Integer(7));
        matriz.get(3).put(new Character('q'), new Integer(4));
        matriz.get(4).put(new Character('b'), new Integer(10));
        matriz.get(5).put(new Character('c'), new Integer(5));
        matriz.get(5).put(new Character('m'), new Integer(9));   
        matriz.get(5).put(new Character('n'), new Integer(6));
        matriz.get(5).put(new Character('o'), new Integer(8));   
        matriz.get(5).put(new Character('p'), new Integer(7));
        matriz.get(5).put(new Character('q'), new Integer(4));
        matriz.get(6).put(new Character('b'), new Integer(10));
        matriz.get(7).put(new Character('b'), new Integer(10));
        matriz.get(8).put(new Character('b'), new Integer(10));
        matriz.get(9).put(new Character('b'), new Integer(10));
        matriz.get(10).put(new Character('c'), new Integer(12));
        matriz.get(10).put(new Character('m'), new Integer(16));   
        matriz.get(10).put(new Character('n'), new Integer(13));
        matriz.get(10).put(new Character('o'), new Integer(15));   
        matriz.get(10).put(new Character('p'), new Integer(14));
        matriz.get(10).put(new Character('q'), new Integer(11));
        matriz.get(11).put(new Character('b'), new Integer(10));
        matriz.get(12).put(new Character('b'), new Integer(10));
        matriz.get(12).put(new Character('c'), new Integer(12));
        matriz.get(13).put(new Character('b'), new Integer(10));
        matriz.get(14).put(new Character('b'), new Integer(10));
        matriz.get(15).put(new Character('b'), new Integer(10));
        matriz.get(16).put(new Character('b'), new Integer(10));
        
    }
    
    public Integer getSiguienteEstado(Integer estado, Character caracter){
        return matriz.get(estado).get(caracter);
    }
    
    public boolean esFinal(Integer estado){
        return estadosFinales.contains(estado);
    }
    

    
    public String añadeLetraRandom(Integer estadoActual){
        HashMap estado = matriz.get(estadoActual);
        Object randomName = estado.keySet().toArray()[new Random().nextInt(estado.keySet().toArray().length)];
    	
        return randomName.toString();
    }
    
    public boolean continuar(Integer estado){
        return !matriz.get(estado).isEmpty();
    }
    
}